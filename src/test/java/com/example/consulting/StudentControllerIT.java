package com.example.consulting;

import com.example.consulting.model.Student;
import com.example.consulting.student.StudentController;
import com.example.consulting.student.StudentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import java.util.List;

import static com.example.consulting.StudentDataProvider.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest(classes = {ConsultingApplication.class})
class StudentControllerIT {

    @Autowired
    private StudentService studentService;
    private MockMvc restMockMvc;
    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
    @Autowired
    private Validator validator;
    @Autowired
    private StudentDataProvider studentDataProvider;
    @Autowired
    private StudentController studentController;
    private Student student;


    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.restMockMvc = MockMvcBuilders.standaloneSetup(studentController)
                .setCustomArgumentResolvers(pageableArgumentResolver)
                .setMessageConverters(jacksonMessageConverter)
                .setValidator(validator).build();
    }

    @BeforeEach
    public void initTest() {

        student = studentDataProvider.createEntity();
    }

    @Test
    @Transactional
    public void createStudent() throws Exception {

        restMockMvc.perform(post("/student")
                                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                .content(TestUtil.convertObjectToJsonBytes(student)))
                .andExpect(status().isCreated());

        List<Student> students = studentService.getAllStudents();
        Student entity = students.get(0);

        assertThat(entity.getId()).isNotNull();
        assertThat(entity.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(entity.getScore()).isEqualTo(DEFAULT_SCORE);
        assertThat(entity.getTeacher_name()).isEqualTo(DEFAULT_TEACHER);
    }

    @Test
    @Transactional
    public void getStudent() throws Exception {

        student = studentService.saveStudent(student);

        restMockMvc.perform(get("/student/{id}" , student.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(student.getId()))
                .andExpect(jsonPath("$.score").value(DEFAULT_SCORE))
                .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
                .andExpect(jsonPath("$.teacher_name").value(DEFAULT_TEACHER));
    }

    @Test
    @Transactional
    public void getAllStudents() throws Exception {
        student = studentService.saveStudent(student);

        restMockMvc.perform(get("/student"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(student.getId())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
                .andExpect(jsonPath("$.[*].score").value(hasItem(DEFAULT_SCORE)))
                .andExpect(jsonPath("$.[*].teacher_name").value(hasItem(DEFAULT_TEACHER)));
    }

    @Test
    @Transactional
    public void updateStudent() throws Exception {
        student = this.studentService.saveStudent(student);
        Student entity = this.studentService.getStudent(student.getId());
        entity = this.studentDataProvider.updateEntity(entity);

        restMockMvc.perform(put("/student/{id}", student.getId())
                                    .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                    .content(TestUtil.convertObjectToJsonBytes(entity)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(student.getId()))
                .andExpect(jsonPath("$.name").value(UPDATED_NAME))
                .andExpect(jsonPath("$.teacher_name").value(UPDATED_TEACHER))
                .andExpect(jsonPath("$.score").value(UPDATED_SCORE));
    }

    @Test
    @Transactional
    public void deleteStudent() throws Exception {
        student = this.studentService.saveStudent(student);
        int databaseSizeBeforeDelete = this.studentService.getAllStudents().size();

        restMockMvc.perform(delete("/student/{id}", student.getId())
                                    .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent());

        List<Student> specificationList = this.studentService.getAllStudents();
        assertThat(specificationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
