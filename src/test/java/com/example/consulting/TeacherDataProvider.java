package com.example.consulting;

import com.example.consulting.model.Teacher;
import org.springframework.stereotype.Component;

@Component
public class TeacherDataProvider {

    public static final String DEFAULT_TEACHER_NAME = "maverick";
    public static final String UPDATED_TEACHER_NAME = "rick";

    public Teacher createEntity() {
        Teacher entity = new Teacher();
        entity.setName(DEFAULT_TEACHER_NAME);
        return entity;
    }

    public Teacher updateEntity(Teacher entity) {
        entity.setName(UPDATED_TEACHER_NAME);
        return entity;
    }
}
