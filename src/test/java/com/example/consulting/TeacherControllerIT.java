package com.example.consulting;

import com.example.consulting.model.Student;
import com.example.consulting.model.Teacher;
import com.example.consulting.teacher.TeacherController;
import com.example.consulting.teacher.TeacherService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import java.util.List;

import static com.example.consulting.TeacherDataProvider.DEFAULT_TEACHER_NAME;
import static com.example.consulting.TeacherDataProvider.UPDATED_TEACHER_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = {ConsultingApplication.class})
public class TeacherControllerIT {

    @Autowired
    private TeacherService teacherService;
    private MockMvc restMockMvc;
    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
    @Autowired
    private Validator validator;
    @Autowired
    private TeacherDataProvider teacherDataProvider;
    @Autowired
    private TeacherController teacherController;
    private Teacher teacher;


    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.restMockMvc = MockMvcBuilders.standaloneSetup(teacherController)
                .setCustomArgumentResolvers(pageableArgumentResolver)
                .setMessageConverters(jacksonMessageConverter)
                .setValidator(validator).build();
    }

    @BeforeEach
    public void initTest() {

        teacher = teacherDataProvider.createEntity();
    }

    @Test
    @Transactional
    public void createTeacher() throws Exception {

        restMockMvc.perform(post("/teacher")
                                    .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                    .content(TestUtil.convertObjectToJsonBytes(teacher)))
                .andExpect(status().isCreated());

        List <Teacher> teachers = teacherService.getAllTeachers();
        Teacher entity = teachers.get(0);

        assertThat(entity.getId()).isNotNull();
        assertThat(entity.getName()).isEqualTo(DEFAULT_TEACHER_NAME);
    }

    @Test
    @Transactional
    public void getTeacher() throws Exception {

        teacher = teacherService.saveTeacher(teacher);

        restMockMvc.perform(get("/teacher/{id}" , teacher.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(teacher.getId()))
                .andExpect(jsonPath("$.name").value(DEFAULT_TEACHER_NAME));
    }

    @Test
    @Transactional
    public void getAllTeachers() throws Exception {
        teacher = teacherService.saveTeacher(teacher);

        restMockMvc.perform(get("/teacher"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(teacher.getId())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_TEACHER_NAME)));
    }

    @Test
    @Transactional
    public void updateTeacher() throws Exception {
        teacher = this.teacherService.saveTeacher(teacher);
        Teacher entity = this.teacherService.getTeacher(teacher.getId());
        entity = this.teacherDataProvider.updateEntity(entity);

        restMockMvc.perform(put("/teacher/{id}", teacher.getId())
                                    .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                    .content(TestUtil.convertObjectToJsonBytes(entity)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(teacher.getId()))
                .andExpect(jsonPath("$.name").value(UPDATED_TEACHER_NAME));
    }

    @Test
    @Transactional
    public void deleteTeacher() throws Exception {
        teacher = this.teacherService.saveTeacher(teacher);
        int databaseSizeBeforeDelete = this.teacherService.getAllTeachers().size();

        restMockMvc.perform(delete("/teacher/{id}", teacher.getId())
                                    .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent());

        List<Teacher> specificationList = this.teacherService.getAllTeachers();
        assertThat(specificationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
