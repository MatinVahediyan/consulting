package com.example.consulting;

import com.example.consulting.model.Student;
import org.springframework.stereotype.Component;

@Component
public class StudentDataProvider {

    public static final String DEFAULT_NAME = "Jack";
    public static final String UPDATED_NAME = "Jack";

    public static final Long DEFAULT_SCORE = 0L;
    public static final Long UPDATED_SCORE = 20L;

    public static final String DEFAULT_TEACHER = "Michel";
    public static final String UPDATED_TEACHER = "Ben";

    public Student createEntity() {
        Student entity = new Student();
        entity.setName(DEFAULT_NAME);
        entity.setScore(DEFAULT_SCORE);
        entity.setTeacher_name(DEFAULT_TEACHER);
        return entity;
    }

    public Student updateEntity(Student entity) {
        entity.setName(UPDATED_NAME);
        entity.setScore(UPDATED_SCORE);
        entity.setTeacher_name(UPDATED_TEACHER);
        return entity;
    }
}
