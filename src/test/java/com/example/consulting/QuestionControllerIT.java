package com.example.consulting;

import com.example.consulting.model.Question;
import com.example.consulting.question.QuestionController;
import com.example.consulting.question.QuestionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import java.util.List;

import static com.example.consulting.QuestionDataProvider.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = {ConsultingApplication.class})
public class QuestionControllerIT {
    @Autowired
    private QuestionService questionService;
    private MockMvc restMockMvc;
    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;
    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
    @Autowired
    private Validator validator;
    @Autowired
    private QuestionDataProvider questionDataProvider;
    @Autowired
    private QuestionController questionController;
    private Question question;


    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.restMockMvc = MockMvcBuilders.standaloneSetup(questionController)
                .setCustomArgumentResolvers(pageableArgumentResolver)
                .setMessageConverters(jacksonMessageConverter)
                .setValidator(validator).build();
    }

    @BeforeEach
    public void initTest() {

        question = questionDataProvider.createEntity();
    }

    @Test
    @Transactional
    public void createQuestion() throws Exception {

        restMockMvc.perform(post("/question")
                                    .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                    .content(TestUtil.convertObjectToJsonBytes(question)))
                .andExpect(status().isCreated());

        List <Question> questions = questionService.getAllQuestions();
        Question entity = questions.get(0);

        assertThat(entity.getId()).isNotNull();
        assertThat(entity.getContext()).isEqualTo(DEFAULT_CONTEXT);
        assertThat(entity.getAnswer1()).isEqualTo(DEFAULT_ANSWER1);
        assertThat(entity.getAnswer2()).isEqualTo(DEFAULT_ANSWER2);
        assertThat(entity.getAnswer3()).isEqualTo(DEFAULT_ANSWER3);
        assertThat(entity.getAnswer4()).isEqualTo(DEFAULT_ANSWER4);
        assertThat(entity.getTrue_answer()).isEqualTo(DEFAULT_TRUE_ANSWER);
    }

    @Test
    @Transactional
    public void getQuestion() throws Exception {

        question = questionService.saveQuestion(question);

        restMockMvc.perform(get("/question/{id}" , question.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(question.getId()))
                .andExpect(jsonPath("$.context").value(DEFAULT_CONTEXT))
                .andExpect(jsonPath("$.answer1").value(DEFAULT_ANSWER1))
                .andExpect(jsonPath("$.answer2").value(DEFAULT_ANSWER2))
                .andExpect(jsonPath("$.answer3").value(DEFAULT_ANSWER3))
                .andExpect(jsonPath("$.answer4").value(DEFAULT_ANSWER4))
                .andExpect(jsonPath("$.true_answer").value(DEFAULT_TRUE_ANSWER));
    }

    @Test
    @Transactional
    public void getAllQuestions() throws Exception {
        question = questionService.saveQuestion(question);

        restMockMvc.perform(get("/question"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(question.getId())))
                .andExpect(jsonPath("$.[*].context").value(hasItem(DEFAULT_CONTEXT)))
                .andExpect(jsonPath("$.[*].answer1").value(hasItem(DEFAULT_ANSWER1)))
                .andExpect(jsonPath("$.[*].answer2").value(hasItem(DEFAULT_ANSWER2)))
                .andExpect(jsonPath("$.[*].answer3").value(hasItem(DEFAULT_ANSWER3)))
                .andExpect(jsonPath("$.[*].answer4").value(hasItem(DEFAULT_ANSWER4)))
                .andExpect(jsonPath("$.[*].true_answer").value(hasItem(DEFAULT_TRUE_ANSWER)));
    }

    @Test
    @Transactional
    public void updateQuestion() throws Exception {
        question = this.questionService.saveQuestion(question);
        Question entity = this.questionService.getQuestion(question.getId());
        entity = this.questionDataProvider.updateEntity(entity);

        restMockMvc.perform(put("/question/{id}", question.getId())
                                    .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                    .content(TestUtil.convertObjectToJsonBytes(entity)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(question.getId()))
                .andExpect(jsonPath("$.context").value(UPDATED_CONTEXT))
                .andExpect(jsonPath("$.answer1").value(UPDATED_ANSWER1))
                .andExpect(jsonPath("$.answer2").value(UPDATED_ANSWER2))
                .andExpect(jsonPath("$.answer3").value(UPDATED_ANSWER3))
                .andExpect(jsonPath("$.answer4").value(UPDATED_ANSWER4))
                .andExpect(jsonPath("$.true_answer").value(UPDATED_TRUE_ANSWER));
    }

    @Test
    @Transactional
    public void deleteQuestion() throws Exception {
        question = this.questionService.saveQuestion(question);
        int databaseSizeBeforeDelete = this.questionService.getAllQuestions().size();

        restMockMvc.perform(delete("/question/{id}", question.getId())
                                    .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent());

        List <Question> specificationList = this.questionService.getAllQuestions();
        assertThat(specificationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
