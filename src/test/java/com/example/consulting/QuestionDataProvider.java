package com.example.consulting;

import com.example.consulting.model.Question;
import org.springframework.stereotype.Component;

@Component
public class QuestionDataProvider {

    public static final String DEFAULT_CONTEXT = "how old is your brother?";
    public static final String UPDATED_CONTEXT = "what are your goals?";

    public static final String DEFAULT_ANSWER1 = "16";
    public static final String UPDATED_ANSWER1 = "cash";

    public static final String DEFAULT_ANSWER2 = "15";
    public static final String UPDATED_ANSWER2 = "LPI";

    public static final String DEFAULT_ANSWER3 = "14";
    public static final String UPDATED_ANSWER3 = "guitar";

    public static final String DEFAULT_ANSWER4 = "13";
    public static final String UPDATED_ANSWER4 = "wife";

    public static final String DEFAULT_TRUE_ANSWER = "13";
    public static final String UPDATED_TRUE_ANSWER = "cash";

    public Question createEntity() {
        Question entity = new Question();
        entity.setContext(DEFAULT_CONTEXT);
        entity.setAnswer1(DEFAULT_ANSWER1);
        entity.setAnswer2(DEFAULT_ANSWER2);
        entity.setAnswer3(DEFAULT_ANSWER3);
        entity.setAnswer4(DEFAULT_ANSWER4);
        entity.setTrue_answer(DEFAULT_TRUE_ANSWER);
        return entity;
    }

    public Question updateEntity(Question entity) {
        entity.setContext(UPDATED_CONTEXT);
        entity.setAnswer1(UPDATED_ANSWER1);
        entity.setAnswer2(UPDATED_ANSWER2);
        entity.setAnswer3(UPDATED_ANSWER3);
        entity.setAnswer4(UPDATED_ANSWER4);
        entity.setTrue_answer(UPDATED_TRUE_ANSWER);
        return entity;
    }
}
