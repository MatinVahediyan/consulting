package com.example.consulting.question;

import com.example.consulting.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


// responsible for data access
@Repository
public interface QuestionRepository extends JpaRepository <Question , Long> {

}
