package com.example.consulting.question;

import com.example.consulting.model.Question;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/question")
public class QuestionController {

    private final QuestionService service;

    public QuestionController(QuestionService service) {this.service = service;}

    @GetMapping("/{id}")
    public Question getQuestion(@PathVariable(name = "id") Long id) {
        return this.service.getQuestion(id);
    }

    @GetMapping
    public List <Question> getAllQuestions() {
        return this.service.getAllQuestions();
    }

    @PostMapping
    public Question saveQuestion(@RequestBody Question question) {
        return this.service.saveQuestion(question);
    }

    @DeleteMapping("/{id}")
    public void deleteQuestion(@PathVariable Long id) {
        this.service.deleteQuestion(id);
    }

    @PutMapping("/{id}")
    public Question updateQuestion(@RequestBody Question question , @PathVariable Long id) {
        return this.service.updateQuestion(question , id);
    }
}
