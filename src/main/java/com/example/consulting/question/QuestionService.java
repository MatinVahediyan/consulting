package com.example.consulting.question;

import com.example.consulting.model.Question;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionService {

    private final QuestionRepository repository;

    public QuestionService(QuestionRepository repository) {this.repository = repository;}

    public Question getQuestion(Long id) {
        return this.repository.getById(id);
    }

    public List <Question> getAllQuestions() {
        return this.repository.findAll();
    }

    public Question saveQuestion(Question question) {
        return this.repository.save(question);
    }

    public void deleteQuestion(Long id) {
        this.repository.deleteById(id);
    }

    public Question updateQuestion(Question question , Long id) {
        List <Question> questions = this.repository.findAll();

        int studentIndex = 0;
        boolean isUpdatedQuestion = false;

        for (int index = 0 ; index < questions.size() ; index ++) {
            if (questions.get(index).getId().equals(question.getId())) {
                studentIndex = index;
                isUpdatedQuestion = true;
                break;
            }
        }

        //invert ----> !isUpdatedQuestion
        if (!isUpdatedQuestion) {
            return null;
        }

        questions.remove(studentIndex);
        // twice add question bug
        questions.add(studentIndex , question);
        this.repository.deleteAll();
        this.repository.saveAll(questions);
        return question;
    }
}
