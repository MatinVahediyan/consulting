package com.example.consulting.student;

import com.example.consulting.model.Student;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

    private final StudentService service;

    public StudentController(StudentService service) {this.service = service;}

    @GetMapping("/{id}")
    public Student getStudent(@PathVariable(name = "id") Long id) {
        return this.service.getStudent(id);
    }

    @GetMapping
    public List <Student> getAllStudents() {
        return this.service.getAllStudents();
    }

    @PostMapping
    public Student saveStudent(@RequestBody Student student) {
        return this.service.saveStudent(student);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable Long id) {
        this.service.deleteStudent(id);
    }

    @PutMapping("/{id}")
    public Student updateStudent(@RequestBody Student student , @PathVariable Long id) {
        return this.service.updateStudent(student , id);
    }
}
