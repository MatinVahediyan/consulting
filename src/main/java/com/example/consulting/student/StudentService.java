package com.example.consulting.student;

import com.example.consulting.model.Student;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    private final StudentRepository repository;

    public StudentService(StudentRepository repository) {this.repository = repository;}

    public Student getStudent(Long id) {
        return this.repository.getById(id);
    }

    public List <Student> getAllStudents() {
        return this.repository.findAll();
    }

    public Student saveStudent(Student student) {
        return this.repository.save(student);
    }

    public void deleteStudent(Long id) {
        this.repository.deleteById(id);
    }

    public Student updateStudent(Student student , Long id) {
        List <Student> students = this.repository.findAll();

        int studentIndex = 0;
        boolean isUpdatedQuestion = false;

        for (int index = 0 ; index < students.size() ; index ++) {
            if (students.get(index).getId().equals(student.getId())) {
                studentIndex = index;
                isUpdatedQuestion = true;
                break;
            }
        }

        // invert ----> !isUpdatedQuestion
        if (!isUpdatedQuestion) {
            return null;
        }

        students.remove(studentIndex);
        // twice add question bug
        students.add(studentIndex , student);
        this.repository.deleteAll();
        this.repository.saveAll(students);
        return student;
    }
}
