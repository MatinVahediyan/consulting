package com.example.consulting.teacher;

import com.example.consulting.model.Teacher;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {

    private final TeacherRepository repository;

    public TeacherService(TeacherRepository repository) {this.repository = repository;}

    public Teacher getTeacher(Long id) {
        return this.repository.getById(id);
    }

    public List<Teacher> getAllTeachers() {
        return this.repository.findAll();
    }

    public Teacher saveTeacher(Teacher teacher) {
        return this.repository.save(teacher);
    }

    public void deleteTeacher(Long id) {
        this.repository.deleteById(id);
    }

    public Teacher updateTeacher(Teacher teacher , Long id) {
        List <Teacher> teachers = this.repository.findAll();

        int teacherIndex = 0;
        boolean isUpdatedTeacher = false;

        for (int index = 0 ; index < teachers.size() ; index ++) {
            if (teachers.get(index).getId().equals(teacher.getId())) {
                teacherIndex = index;
                isUpdatedTeacher = true;
                break;
            }
        }

        // invert ----> !isUpdatedQuestion
        if (!isUpdatedTeacher) {
            return null;
        }

        teachers.remove(teacherIndex);
        // twice add question bug
        teachers.add(teacherIndex , teacher);
        this.repository.deleteAll();
        this.repository.saveAll(teachers);
        return teacher;
    }
}
