package com.example.consulting.teacher;

import com.example.consulting.model.Teacher;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher")
public class TeacherController {

    private final TeacherService service;

    public TeacherController(TeacherService service) {this.service = service;}

    @GetMapping("/{id}")
    public Teacher getTeacher(@PathVariable(name = "id") Long id) {
        return service.getTeacher(id);
    }

    @GetMapping
    public List <Teacher> getAllTeachers() {
        return this.service.getAllTeachers();
    }

    @PostMapping
    public Teacher saveTeacher(@RequestBody Teacher teacher) {
        return this.service.saveTeacher(teacher);
    }

    @DeleteMapping("/{id}")
    public void deleteTeacher(@PathVariable(name = "id") Long id) {
        this.service.deleteTeacher(id);
    }

    @PutMapping("/{id}")
    public Teacher updateTeacher(@RequestBody Teacher teacher , @PathVariable(name = "id") Long id) {
        return this.service.updateTeacher(teacher , id);
    }
}