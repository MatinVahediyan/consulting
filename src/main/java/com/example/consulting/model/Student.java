package com.example.consulting.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_id" , nullable = false)
    private Long id;

    @Column(name = "student_name" , nullable = false)
    private String name;

    @Column(name = "score")
    private Long score;

    @Column(name = "teacher_name")
    private String teacher_name;
}
