package com.example.consulting.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "question")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "question_id" , nullable = false)
    private Long id;

    @Column(name = "context" , nullable = false)
    private String context;

    @Column(name = "answer1" , nullable = false)
    private String answer1;

    @Column(name = "answer2" , nullable = false)
    private String answer2;

    @Column(name = "answer3" , nullable = false)
    private String answer3;

    @Column(name = "answer4" , nullable = false)
    private String answer4;

    @Column(name = "true_answer" , nullable = false)
    private String true_answer;
}