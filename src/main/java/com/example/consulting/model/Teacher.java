package com.example.consulting.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "teacher")
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "teacher_id" , nullable = false)
    private Long id;

    @Column(name = "teacher_name" , nullable = false)
    private String name;
}